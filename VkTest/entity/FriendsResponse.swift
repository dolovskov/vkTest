//
//  friendsResponse.swift
//  VkTest
//
//  Created by Vladislav Dolovskov on 21.08.17.
//  Copyright © 2017 Vladislav Dolovskov. All rights reserved.
//

import Foundation
import ObjectMapper

struct FriendsResponse : Mappable {
    
    var response : [User]?
    
    init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    mutating func mapping(map: Map) {
        response       <- map["response"]
    }
    
}
