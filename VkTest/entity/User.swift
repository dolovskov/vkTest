//
//  User.swift
//  VkTest
//
//  Created by Vladislav Dolovskov on 21.08.17.
//  Copyright © 2017 Vladislav Dolovskov. All rights reserved.
//

import Foundation
import ObjectMapper

struct User : Mappable {
    
    var id : Int?
    var firstName : String?
    var lastName : String?
    var imageUrl : String?
    
    init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    mutating func mapping(map: Map) {
        id                  <- map["user_id"]
        firstName           <- map["first_name"]
        lastName            <- map["last_name"]
        imageUrl            <- map["photo_50"]
        
    }
    
    
}
