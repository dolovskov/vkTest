//
//  FriendTableViewController.swift
//  VkTest
//
//  Created by Vladislav Dolovskov on 21.08.17.
//  Copyright © 2017 Vladislav Dolovskov. All rights reserved.
//

import UIKit

class FriendTableViewController: UITableViewController {
    
    var friends = [User]()
    
    var loadMoreStatus = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.hidesWhenStopped = true
        loadMore()
    }
    
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let deltaOffset = maximumOffset - currentOffset
        
        if maximumOffset > 0 {
            if deltaOffset <= 0 {
                loadMore()
            }
        }

    }
    
    
    func loadMore() {
        if ( !loadMoreStatus ) {
            self.loadMoreStatus = true
            self.activityIndicator.startAnimating()
            self.tableView.tableFooterView?.isHidden = false
            
            ApiClient.shared.getFriend(byUserId: 14948357, offset: friends.count, {
                friends in
                self.friends += friends
                self.activityIndicator.stopAnimating()
                self.tableView.tableFooterView?.isHidden = true
                self.tableView.reloadData()
                self.loadMoreStatus = false
            }, { sCode in
                self.activityIndicator.stopAnimating()
                self.tableView.tableFooterView?.isHidden = true
                self.showError(sCode: sCode)
                self.loadMoreStatus = false
            })

            
            
        }
    }

    
    func showError(sCode: Int) {
        let errorAlert = UIAlertController(title: "Ошибка \(sCode)", message: "", preferredStyle: .alert)
        let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
        errorAlert.addAction(okButton)
        self.present(errorAlert, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return friends.count
    }
    
    
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
     
        cell.textLabel?.text = (friends[indexPath.row].firstName ?? "") + " " + (friends[indexPath.row].lastName ?? "")
     
        return cell
     }
     
    
    
}
