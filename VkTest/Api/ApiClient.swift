//
//  ApiClient.swift
//  VkTest
//
//  Created by Vladislav Dolovskov on 21.08.17.
//  Copyright © 2017 Vladislav Dolovskov. All rights reserved.
//

import Foundation
import Alamofire

class ApiClient {
    
    static let shared = ApiClient()
    
    
    func getFriend(byUserId id: Int, offset: Int, _ onCompetion: @escaping ([User]) -> Void,_ onError: @escaping (Int) -> Void) {
        Alamofire.SessionManager.default.request("https://api.vk.com/method/friends.get", method: .get, parameters: ["user_id":id, "offset":offset,"count" : 20, "fields" : "photo_50", "order":"name", "version" : 5.8]).validate().responseString(completionHandler: {
            response in
            if response.result.isSuccess {
                let friendsResponse = FriendsResponse(JSONString: response.result.value ?? "")
                let friends = friendsResponse?.response ?? [User]()
                onCompetion(friends)
            } else {
                onError(response.response?.statusCode ?? 0)
            }
        })
    }
    
}
